//
//  TopViewController.m
//  kreativa5
//
//  Created by Kosta Mihajlov on 11/16/13.
//  Copyright (c) 2013 Halicea.Co. All rights reserved.
//

#import "TopViewController.h"
#import "CatCell.h"
#import "ProgramsViewController.h"
#import "DownloadHelper.h"
@interface TopViewController ()  <UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property(nonatomic, strong) NSDictionary* map;
@property NSInteger category;
@property NSInteger lastSelected;
@property (nonatomic, strong) NSString* name;
@property(nonatomic, strong) NSArray* titles;
@property bool isCellViewLoaded;
@end

@implementation TopViewController
@synthesize topElements;
@synthesize topNAv;
@synthesize map;

- (void)viewDidLoad
{
    self.navigationController.navigationBar.alpha = 0.75;
    [super viewDidLoad];
    [self setupTopCategories];
    self.isCellViewLoaded = NO;
    self.collectionView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_app.jpg"]];
    [self.navigationItem setTitle:self.name];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self.category = -1;
    self.lastSelected = -1;
    self.map = nil;
    return [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self.category = -1;
    self.lastSelected = -1;
    self.map = nil;
    self.name = [NSString alloc];
    
    return [super initWithCoder:aDecoder];
}
- (void)setupTopCategories{
    self.map = [TopViewController dictionaryWithContentsOfJSONString:@"config.json"];
    NSArray *rootTitles=[[[self.map objectForKey:@"sections"] objectAtIndex:0] objectForKey:@"elements"];
    if (self.category>=0) {
        self.titles = [[[[rootTitles objectAtIndex:self.category] objectForKey:@"sections"] objectAtIndex:0] objectForKey:@"elements"];
        self.name = [[rootTitles objectAtIndex:self.category] objectForKey:@"title"];
        
    }else{
        self.titles = rootTitles;
        self.name = @"СЕ ГРИЖИМЕ...";
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    //WE ARE FIRING THE SEGUES MANUALY
    return NO;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier  );
    if ([segue.identifier isEqualToString:@"toSub"])
    {
        TopViewController *sub = segue.destinationViewController;
        sub.category = self.lastSelected;
        NSLog(@"Category= %ld",(long)sub.category);
    }else{
        ProgramsViewController *p = segue.destinationViewController;
        p.url = [[self.titles objectAtIndex:self.lastSelected] objectForKey:@"url"];
        [p setTitle:[[self.titles objectAtIndex:self.lastSelected] objectForKey:@"caption"]];
    }
}
#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.titles count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

// 3

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(!self.isCellViewLoaded){
        UINib *rc = [UINib nibWithNibName:@"cellView" bundle: nil];;
        [cv registerNib:rc forCellWithReuseIdentifier:@"MyCell"];
    }
    CatCell *cell = (CatCell *)[cv dequeueReusableCellWithReuseIdentifier:@"MyCell" forIndexPath:indexPath];
    NSString *cellTitle =@"title";
    if(self.category>=0){
        cellTitle = @"caption";
        NSString *title = (NSString *)[[self.titles objectAtIndex:[indexPath row]] objectForKey:cellTitle];
        [cell setTitleFromString:title];
    }else{
        int index = [indexPath row];
        index+=1;
        NSString *imgName = [NSString stringWithFormat:@"p%d.png", index];
        UIImage *img = [UIImage imageNamed:imgName];
        [cell.imBackground setImage: img];
        [cell setTitleFromString:@""];
    }
    
    
    

    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *r = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"top_header" forIndexPath:indexPath];
    return r;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    self.lastSelected =  [indexPath row];
    if(self.category>=0){
        [self performSegueWithIdentifier:@"toWeb" sender:self];
    }else{
        [self performSegueWithIdentifier:@"toSub" sender:self];
    }
    NSLog(@"Just selected something %ld", (long)self.lastSelected);
    // TODO: Select Item
}

+(NSDictionary*)dictionaryWithContentsOfJSONString:(NSString*)fileLocation{
    [DownloadHelper refreshConfigIfNeeded];
    NSData* data = [NSData dataWithContentsOfFile:[DownloadHelper getConfigFilePath]];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
   
    if (error != nil) return nil;
    return result;
}

#pragma mark – UICollectionViewDelegateFlowLayout


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
