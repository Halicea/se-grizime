//
//  TopViewController.h
//  kreativa5
//
//  Created by Kosta Mihajlov on 11/16/13.
//  Copyright (c) 2013 Halicea.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopViewController : UICollectionViewController
@property (weak, nonatomic) IBOutlet UINavigationItem *topNAv;
@property (strong, nonatomic) IBOutlet UICollectionView *topElements;
@end
