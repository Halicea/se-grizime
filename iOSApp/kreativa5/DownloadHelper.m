//
//  DownloadHelper.m
//  kreativa5
//
//  Created by Kosta Mihajlov on 11/24/13.
//  Copyright (c) 2013 Halicea.Co. All rights reserved.
//

#import "DownloadHelper.h"

@implementation DownloadHelper
+(bool) isUrlModified:(NSURL*)url since:(NSDate*)lastUpdateTime{
    // create a HTTP request to get the file information from the web server
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    
    NSHTTPURLResponse* response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    // get the last modified info from the HTTP header
    NSString* httpLastModified = nil;
    if ([response respondsToSelector:@selector(allHeaderFields)])
    {
        httpLastModified = [[response allHeaderFields]
                            objectForKey:@"Last-Modified"];
    }
    
    // setup a date formatter to query the server file's modified date
    // don't ask me about this part of the code ... it works, that's all I know :)
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"EEE',' dd MMM yyyy HH':'mm':'ss 'GMT'";
    df.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    df.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    // test if the server file's date is later than the local file's date
    NSDate* serverFileDate = [df dateFromString:httpLastModified];
    
    NSLog(@"Local File Date: %@ Server File Date: %@",lastUpdateTime,serverFileDate);
    //If file doesn't exist, download it
    if(lastUpdateTime==nil){
        return YES;
    }
    return ([lastUpdateTime laterDate:serverFileDate] == serverFileDate);
}

+(void) downloadFileFromUrl:(NSURL*)url to:(NSString*)filePath{
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        [urlData writeToFile:filePath atomically:YES];
    }
}

+(void) refreshConfigIfNeeded{
    NSString *configUrl =  @"http://kreativa5.halicea.com/config.json";
    NSURL *url = [[NSURL alloc] initWithString:configUrl];
    NSString  *filePath = [self getConfigFilePath];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if(fileExists){
        NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        NSDate *dateModified = [attributes fileModificationDate];
        if ([self isUrlModified:url since:dateModified]) {
            [self downloadFileFromUrl: url to:filePath];
        }
    }else{
        [self downloadFileFromUrl: url to:filePath];
    }
}
+(NSString*) getConfigFilePath{
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    return [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"config.json"];
}
@end
