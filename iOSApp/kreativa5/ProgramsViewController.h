//
//  ProgramsControllersViewController.h
//  kreativa5
//
//  Created by Kosta Mihajlov on 11/17/13.
//  Copyright (c) 2013 Halicea.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramsViewController : UIViewController
@property (strong, nonatomic) NSString* url;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
