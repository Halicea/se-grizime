//
//  CatCell.h
//  kreativa5
//
//  Created by Kosta Mihajlov on 11/17/13.
//  Copyright (c) 2013 Halicea.Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatCell : UICollectionViewCell

-(void) setTitleFromString:(NSString *) title;

@property (weak, nonatomic) IBOutlet UILabel *laTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imBackground;

@end
