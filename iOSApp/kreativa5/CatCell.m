//
//  CatCell.m
//  kreativa5
//
//  Created by Kosta Mihajlov on 11/17/13.
//  Copyright (c) 2013 Halicea.Co. All rights reserved.
//

#import "CatCell.h"

@implementation CatCell
@synthesize laTitle;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    [self.layer setShadowOffset:CGSizeMake(0, 2)];
    [self.layer setShadowRadius:1.0];
    [self.layer setShadowColor:[UIColor whiteColor].CGColor] ;
    [self.layer setShadowOpacity:0.5];
    [self.layer setShadowPath:[[UIBezierPath bezierPathWithRect:self.bounds] CGPath]];
    return self;
}

-(void) setTitleFromString:(NSString*)string{
    [self.laTitle setText:string];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
