//
//  ProgramsControllersViewController.m
//  kreativa5
//
//  Created by Kosta Mihajlov on 11/17/13.
//  Copyright (c) 2013 Halicea.Co. All rights reserved.
//

#import "ProgramsViewController.h"

@interface ProgramsViewController ()<UIWebViewDelegate>

@end

@implementation ProgramsViewController
@synthesize webView;
@synthesize url;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_app.jpg"]]];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    [self setTitle:self.title];
    self.webView.scalesPageToFit = YES;
    [self.webView loadRequest:req];
    self.webView.delegate = self;
    
	// Do any additional setup after loading the view.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"Just loaded myself");
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSLog(@"Just started loading");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.webView stringByEvaluatingJavaScriptFromString:@"var e = document.createEvent('Events'); "
     @"e.initEvent('orientationchange', true, false);"
     @"document.dispatchEvent(e); "];
}

@end
