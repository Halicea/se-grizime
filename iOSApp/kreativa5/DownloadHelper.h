//
//  DownloadHelper.h
//  kreativa5
//
//  Created by Kosta Mihajlov on 12/6/13.
//  Copyright (c) 2013 Halicea.Co. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DownloadHelperDelegate <NSObject>
@optional
- (void) didReceiveData: (NSData *) theData;
- (void) didReceiveFilename: (NSString *) aName;
- (void) dataDownloadFailed: (NSString *) reason;
- (void) dataDownloadAtPercent: (NSNumber *) aPercent;
@end

@interface DownloadHelper : NSObject
    +(bool) isUrlModified:(NSURL*)url since:(NSDate*)lastUpdateTime;
    +(void) downloadFileFromUrl:(NSURL*)url to:(NSString*)filePath;
    +(void)refreshConfigIfNeeded;
    +(NSString*) getConfigFilePath;
@end
