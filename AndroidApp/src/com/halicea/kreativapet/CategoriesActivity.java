package com.halicea.kreativapet;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.ViewById;
import com.halicea.kreativapet.domain.AppContent;
import com.halicea.kreativapet.domain.KreativaDownloadListener;


@EActivity(R.layout.activity_categories)

public class CategoriesActivity extends Activity implements KreativaDownloadListener{
	@ViewById
	AbsListView gridview;

	@Bean
	AppContent contentProvider;
	
	@Extra
	int categoryIndex=-1;
	
	@AfterViews
	void setGrid() {
		final CategoriesActivity self = this;
		setAdapter();
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				if(categoryIndex<0){
					SubCategoriesActivity_.intent(self).categoryIndex(position).start();
				}else{
					WebViewActivity_.intent(self).categoryIndex(categoryIndex).subCategoryIndex(position).start();
				}
			}
		});
	}
	void setAdapter(){
		JSONObject structure = contentProvider.getStructure();
		if(structure!=null){
			if(categoryIndex>=0){
				JSONObject obj = contentProvider.getObject(categoryIndex);
				try {
					setTitle(obj.getString("title"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			((AdapterView)gridview).setAdapter(new CategoriesAdapter(this, structure, categoryIndex));
		
		}
	}
	
	@Override
	protected void onPause() {
		contentProvider.removeListener(this);
		super.onPause();
	}
	@Override
	protected void onResume() {
		contentProvider.addListener(this);
		super.onResume();
	}


	@Override
	public void onConfigDownloadFinished() {
		setAdapter();
	}
	

	@Override
	public void onPageDownloadFinished(int pageIndex) {
	}
}
