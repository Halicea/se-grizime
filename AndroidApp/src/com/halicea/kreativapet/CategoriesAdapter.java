package com.halicea.kreativapet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

public class CategoriesAdapter implements ListAdapter {

	private CategoriesActivity categoriesActivity;
	private JSONObject structure;
	private int subcatIndex;

	public CategoriesAdapter(CategoriesActivity categoriesActivity,
			JSONObject structure, int subcatIndex) {

		this.categoriesActivity = categoriesActivity;
		this.structure = structure;
		this.subcatIndex = subcatIndex;
	}

	private JSONArray getElements() {
		try {
			JSONArray topLevel = structure.getJSONArray("sections")
					.getJSONObject(0).getJSONArray("elements");
			if (this.subcatIndex < 0) {
				return topLevel;
			} else {
				return topLevel.getJSONObject(this.subcatIndex)
						.getJSONArray("sections").getJSONObject(0)
						.getJSONArray("elements");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new JSONArray();
		}
	}

	private long hashForString(String string) {
		long hash = 7;
		for (int i = 0; i < string.length(); i++) {
			hash = hash * 31 + string.charAt(i);
		}
		return hash;
	}

	@Override
	public int getCount() {
		try {
			return getElements().length();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		try {
			return getElements().getJSONObject(position);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		try {
			String title = "";
			if (getElements().getJSONObject(position).has("title")) {
				title = getElements().getJSONObject(position)
						.getString("title");
			} else {
				title = getElements().getJSONObject(position).getString(
						"caption");
			}
			return hashForString(title);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View result;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater li = (LayoutInflater) this.categoriesActivity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			result = li.inflate(R.layout.grid_cell, null);
		} else {
			result = (View) convertView;
		}
		TextView tv = (TextView) result.findViewById(R.id.grid_item_text);
		try {
			JSONObject item = null;
			item = getElements().getJSONObject(position);
			if(this.subcatIndex<0){
				int imageResource = categoriesActivity.getResources()
						.getIdentifier(String.format("drawable/p%s", position + 1),
								null, categoriesActivity.getPackageName());
				Drawable image = categoriesActivity.getResources().getDrawable(
						imageResource);
				result.setBackgroundDrawable(image);
			}else{
				tv.setText(item.getString("caption"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return false;
	}

}