package com.halicea.kreativapet.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Environment;

import com.googlecode.androidannotations.annotations.EBean;

@EBean
public class AppContent {
	RefreshConfig rc;
	public static String getConfigPath(){
		String result =Environment.getExternalStorageDirectory().toString()+"/.grizime.json";
		return result;
	}
	
	private JSONObject json = null;
	public AppContent(){
		rc = new RefreshConfig();
	}
	private void downloadConfig(){
		rc.execute();
	}
	public void addListener(KreativaDownloadListener l){
		rc.addListener(l);
	}
	public void removeListener(KreativaDownloadListener l){
		rc.removeListener(l);
	}
	private boolean shouldDownloadConfig(){
		if(!rc.isRunning){
			File f = new File(AppContent.getConfigPath());
			if (f.exists()){
				return false;
			}else{
				return true;
			}
		}
		return false;
	}
	private String getJSONString() {
		try {
			File f = new File(AppContent.getConfigPath());
			if (f.exists()){
				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(AppContent.getConfigPath()), "UTF-16"));
				String json = reader.readLine();
				reader.close();
				return json;
			}else{
				return null;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONObject getStructure() {
		
		if(json==null){
			try {
				if(shouldDownloadConfig()){
					downloadConfig();
				}else{
					try{
					json = new JSONObject(getJSONString());
					}catch(NullPointerException ex){
						json=null;
					}
				}
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return json;
	}

	private JSONArray getElementsArray(JSONObject obj) {
		try {
			return obj.getJSONArray("sections").getJSONObject(0)
					.getJSONArray("elements");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONObject getObject(){
		return getObject(-1, -1);
	}
	public JSONObject getObject(int catindex){
		return getObject(catindex, -1);
	}
	public JSONObject getObject(int catindex, int subCatIndex) {
		if (catindex >= 0) {
			try{
				JSONObject o = getElementsArray(getStructure()).getJSONObject(catindex);
				if (subCatIndex >= 0) {
					return getElementsArray(o).getJSONObject(subCatIndex);
	
				} else {
					return o;
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
		}
		return getStructure();
	}
}
