package com.halicea.kreativapet.domain;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Background Async Task to download file
 * */
public class RefreshConfig extends AsyncTask<Void, Void, Void> {
	
	private ArrayList<KreativaDownloadListener> listeners = new ArrayList<KreativaDownloadListener>();
	public boolean isRunning = false;
	public void addListener(KreativaDownloadListener l){
		if(!listeners.contains(l)){
			listeners.add(l);
		}
	}
	public void removeListener(KreativaDownloadListener l){
		if(listeners.contains(l)){
			listeners.remove(l);
		}
	}
	@Override
	protected void onPreExecute() {
		isRunning = true;
		super.onPreExecute();
	}
    @Override
    protected Void doInBackground(Void ...params) {
    	
        int count;
        try {
        	URL url = new URL("http://kreativa5.halicea.com/config.json");
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            @SuppressWarnings("unused")
			long lm = conection.getLastModified();
            
            int lenghtOfFile = conection.getContentLength();
            // download the file
            InputStream input = new BufferedInputStream(url.openStream());

            // Output stream
            OutputStream output = new FileOutputStream(AppContent.getConfigPath());

            byte data[] = new byte[1024];

            @SuppressWarnings("unused")
			long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return null;
    }
    
    @Override
    protected void onPostExecute(Void v) {
    	for (int i = 0; i < listeners.size(); i++) {
			listeners.get(i).onConfigDownloadFinished();
		}
    	isRunning = false;
    }
}