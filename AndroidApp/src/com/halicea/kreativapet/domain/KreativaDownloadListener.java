package com.halicea.kreativapet.domain;

public interface KreativaDownloadListener{
	void onConfigDownloadFinished();
	void onPageDownloadFinished(int pageIndex);
}
