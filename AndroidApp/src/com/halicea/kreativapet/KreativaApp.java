package com.halicea.kreativapet;

import android.content.Context;

public class KreativaApp extends android.app.Application {

    private static KreativaApp instance;

    public KreativaApp() {
    	instance = this;
    }

    public static Context getContext() {
    	return instance;
    }

}