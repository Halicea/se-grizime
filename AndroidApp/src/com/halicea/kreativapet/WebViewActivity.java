package com.halicea.kreativapet;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.webkit.WebView;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.ViewById;
import com.halicea.kreativapet.domain.AppContent;

@EActivity(R.layout.detail)
public class WebViewActivity extends Activity {
	
	@Bean
	AppContent contentProvider;
	
	@ViewById
	WebView webView;
	
	@Extra
	int categoryIndex;
	@Extra
	int subCategoryIndex;
	
	@AfterViews
	void setWebPage(){
		
		JSONObject pageInfo = contentProvider.getObject(categoryIndex, subCategoryIndex);
		try {
			setTitle(pageInfo.getString("caption"));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			webView.loadUrl(pageInfo.getString("url"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
