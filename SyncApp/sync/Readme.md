# Kreativa5 Mobile Sync Script
## Installation
### Installation(Unix)
  - open terminal:
    > sudo easy_install beautifulsoup4

    > sudo easy_install pyrax

    > sudo easy_install mechanize

### Installation(Windows)
  - install python 
  - instal easy_install for python:
    - download <a href="https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py"> ez_setup.py</a>
    - run it
    > shell> cd /path/to/ez_setup.py </br>
    > shell> python ez_setup.py

  - navigate to the installation dir and run
    > shell>setup.cmd

## Usage
  - To update only new content run:
  > shell>python kreativa_sync.py

  - To refresh everything run:
  > shell>python kreativa_sync.py -f

  Once the script is finihed you can check the pages on:

  <a href="http://kreativa5.halicea.com/1.html">http://kreativa5.halicea.com/[1.html, 2.html .. , 91.html]</a>

  The entire structure of the site is saved under:

  <a href="http://kreativa5.halicea.com/config.json">http://kreativa5.halicea.com/config.json</a>


## Update the mobile page Template
  
  Edit the file **page_template.html**.

  **Note** Do not remove the two '%s' strings since they are placeholders for the title and content of the page 

## Important

  In the package there is a file called '**rackspace_credentials.conf**'
  
  **Make sure to keep it safe :)**


  
