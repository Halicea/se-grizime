#!/usr/local/bin/env python
# -*- coding: utf-8 -*-
import codecs
import copy
import datetime
import json
import os
import re
import time
import pyrax

from bs4 import BeautifulSoup
import mechanize
import sys

pyrax.set_setting("identity_type", "rackspace")
creds_file = "./rackspace_credentials.conf"
pyrax.set_credential_file(creds_file)
cf = pyrax.cloudfiles
cont = cf.create_container("kreativa5")

base_url = 'http://creativa5.com/dev/giz'
html_template = None

rootcat = {
    "title": "Креатива 5",
    "type":"root",
    "sections":[
      {
        "elements":[]
      }

    ]
}

cat = {
    "title": "Програми",
    "type":"root",
    "sections":[
      {
        "elements":[]
      }

    ]
}

leaf =  {"caption":"" , "type":"html", "url":""}
host_url = 'http://b122d7d341d3f2fa918f-3cf705082af0b7908b4fecbde6c60a3a.r38.cf5.rackcdn.com/'
class KreativaParser(object):

  def __init__(self, force_reupload=False):
    self.force_reupload = force_reupload
    self.start_time = datetime.datetime.now
    self.html_dir = os.path.expanduser('~/kreativa_html')
    if not os.path.exists(self.html_dir):
      os.makedirs(self.html_dir)

    self.traversed = []
    self.items_for_upload = []
    self.result = copy.deepcopy(rootcat)
    self.b = mechanize.Browser()
    self.categories = [ {'text':'ДОМУВАЊЕ', 'url':'http://creativa5.com/dev/giz/programi_za_domuvanje_list'},
                       {'text':'ХРАНА И ИСХРАНА', 'url':'http://creativa5.com/dev/giz/programi_za_hrana_i_ishrana'},
                       {'text':'ЗДРАВСТВЕНА ЗАШТИТА', 'url':'http://creativa5.com/dev/giz/programi_za_zdravstvena_zastita'},
                       {'text':'ВРАБОТУВАЊЕ', 'url':'http://creativa5.com/dev/giz/programi_za_vrabotuvanje'},
                       {'text':'ОБРАЗОВАНИЕ', 'url':'http://creativa5.com/dev/giz/programi_za_obrazovanie'},
                       {'text':'ДЕТСКА И СОЦИЈАЛНА ЗАШТИТА', 'url':'http://creativa5.com/dev/giz/programi_za_detska_i_socijalna_zastita'},
                       {'text':'ПРАВА ПРЕДВИДЕНИ ОД СТРАНА НА ФПИО', 'url':'http://creativa5.com/dev/giz/programi_za_prava_predvideni_od_fpio'},
                       {'text':'ЗЕМЈОДЕЛСТВО', 'url':'http://creativa5.com/dev/giz/programi_za_zemjodelstvo'}]
    self.skip_matches = [u'За Проектот', u'Листа на кратенки', u'Контакт']
  def should_update(self, headers, file_path):
    if self.force_reupload:
      return True
    elif not os.path.exists(file_path):
      return True
    else:
      if headers.has_key('Last-Modified'):
        web_last_modified = datetime.datetime.strptime(headers['Last-Modified'], '%a, %d %b %Y %H:%M:%S GMT')
        last_refreshed = time.ctime(os.path.getmtime(file_path))
        last_refreshed = datetime.datetime.strptime(last_refreshed, "%a %b %d %H:%M:%S %Y")
        return last_refreshed<web_last_modified
      return True

  def should_skip(self, url, printt=True):
    skipped = False
    for k in self.skip_matches:
      if k in url.text:
        skipped = True
    if not skipped:
      if url.url.startswith('#'):
        skipped = True
      elif not '/node/' in url.url:
        skipped = True
      if url.url in self.traversed:
        skipped = True
      if '[' in url.text:
        skipped = True
#     if not skipped:
#       print skipped , url.text, url.url
    return skipped

  def sanitized_html(self, title, html_table):
    html_table = re.sub(r'<table [^>]*', '<table data-role="table" id="my-table" data-mode="reflow"', html_table)
    html_table = re.sub(r'width=".*"', '', html_table)
    return html_template%(title, html_table)

  def refresh_pages(self):
    count = 0
    for k in self.categories:
      c = copy.deepcopy(cat)
      c['title'] =k['text']
      self.result['sections'][0]['elements'].append(c)
      result = self.b.open(k['url'])
      self.traversed.append(k)

      for l in [x for x in self.b.links() if not self.should_skip(x)]:
        self.b.follow_link(l)
        self.traversed.append(l.url)
        self.traversed.append(l.url)
        res = self.b.follow_link(l)
        output_file = os.path.join(self.html_dir, '%s.html' % count)
        if self.should_update(res.info(), output_file):
          soup = BeautifulSoup(res.read())
          result = soup.find('table')

          leafc = copy.deepcopy(leaf)
          leafc['caption'] = l.text
          leafc['origin_url'] = self.b.geturl()
          leafc['url']=host_url+"%s.html"%count
          c['sections'][0]['elements'].append(leafc)

          html_table = repr(result)
          if html_table:
            html = self.sanitized_html(l.text, html_table)
            f = codecs.open(output_file, 'w', 'utf-16')
            f.write(html)
            f.close()
          else:
            print 'Can\'t find table'
          print count, l.text, l.url
          self.items_for_upload.append(os.path.basename(output_file))
        count += 1

    json_path = os.path.join(self.html_dir, 'config.json')
    old_json = codecs.open(json_path, 'r', 'utf-16').read()
    new_json = json.dumps(self.result)

    if old_json!=new_json or self.force_reupload:
      self.items_for_upload.append(os.path.basename(json_path))
      ress = codecs.open(json_path, 'w', 'utf-16')
      ress.write(new_json)
      ress.close()
    print self.traversed

  def upload_changes(self):
    uploaded = 0
    for f in self.items_for_upload:
      content_type = f.split(".")[-1] == 'html' and 'text/html' or 'application/json'
      cf.upload_file('kreativa5', os.path.join(self.html_dir, f), content_type=content_type)
      cf.purge_cdn_object(cont, f)
      uploaded+=1
      print "Progress: %4.2f%%" % ((uploaded * 100.0) / len(self.items_for_upload))

if __name__ == '__main__':
  global html_template
  html_template = codecs.open(os.path.join(os.path.dirname(__file__),'page_template.html'), 'r', 'utf-8').read()
  force_reupload = '-f' in sys.argv
  kp = KreativaParser(force_reupload)
  kp.refresh_pages()
  kp.upload_changes()
